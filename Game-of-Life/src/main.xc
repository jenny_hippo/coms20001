// COMS20001 - Cellular Automaton Farm - Initial Code Skeleton
// (using the XMOS i2c accelerometer demo code)

#include <platform.h>
#include <xs1.h>
#include <stdio.h>
#include "pgmIO.h"
#include "i2c.h"

#define  IMHT 16                  //image height
#define  IMWD 16                 //image width
#define  DEAD 0x00                //pixel value for dead cell
#define  LIVE 0xFF                //pixel value for live cell

//LEDS
#define  SGREEN 1                 //separating green for LED
#define  BLUE 2
#define  GREEN 4
#define  RED 8

#define  TEST 1  // 1 to speed up processing, 2 to export round 2 and 1000

#define  NUMWORKERS 4

typedef unsigned char uchar;      //using uchar as shorthand

on tile[0]: port p_scl = XS1_PORT_1E;         //interface ports to orientation
on tile[0]: port p_sda = XS1_PORT_1F;
on tile[0] : in port buttons = XS1_PORT_4E; //port to access xCore-200 buttons
on tile[0] : out port leds = XS1_PORT_4F;   //port to access xCore-200 LEDs

#define FXOS8700EQ_I2C_ADDR 0x1E  //register addresses for orientation
#define FXOS8700EQ_XYZ_DATA_CFG_REG 0x0E
#define FXOS8700EQ_CTRL_REG_1 0x2A
#define FXOS8700EQ_DR_STATUS 0x0
#define FXOS8700EQ_OUT_X_MSB 0x1
#define FXOS8700EQ_OUT_X_LSB 0x2
#define FXOS8700EQ_OUT_Y_MSB 0x3
#define FXOS8700EQ_OUT_Y_LSB 0x4
#define FXOS8700EQ_OUT_Z_MSB 0x5
#define FXOS8700EQ_OUT_Z_LSB 0x6

#define  INTSIZE (sizeof(int) * 8)
#define  BPHEIGHT (1 + ((IMHT - 1) / INTSIZE)) // equivalent of ceiling(IMHT, INTSIZE)

#define OVERFLOWTIME (2^32 - 1)

//DISPLAYS an LED pattern
int showLEDs(out port p, chanend fromDistributor) {
  int pattern; //1st bit...separate green LED
               //2nd bit...blue LED
               //3rd bit...green LED
               //4th bit...red LED
  while (1) {
    fromDistributor :> pattern;   //receive new pattern from distributor
    p <: pattern;                //send pattern to LED port
  }
  return 0;
}

//READ BUTTONS and send button pattern to Distributor
void buttonListener(in port b, chanend toDistributor) {
  int r;
  while (1) {
    b when pinseq(15)  :> r;    // check that no button is pressed
    b when pinsneq(15) :> r;    // check if some buttons are pressed
    if ((r==13) || (r==14))     // if either button is pressed
    toDistributor <: r;         // send button pattern to distributor
    else toDistributor <: 0;
  }
}

/////////////////////////////////////////////////////////////////////////////////////////
//
// Read Image from PGM file from path infname[] to channel c_out
//
/////////////////////////////////////////////////////////////////////////////////////////

void DataInStream(char infname[], chanend c_out)
{
  int res;
  uchar line[ IMWD ];
  printf( "DataInStream: Start...\n" );

  //Open PGM file
  res = _openinpgm( infname, IMWD, IMHT );
  if( res ) {
    printf( "DataInStream: Error opening %s\n.", infname );
    return;
  }

  //Read image line-by-line and send byte by byte to channel c_out
  for( int y = 0; y < IMHT; y++ ) {
    _readinline( line, IMWD );

    for( int x = 0; x < IMWD; x++ ) {
        c_out <: line[ x ];
    }
  }

  //Close PGM image file
  _closeinpgm();
  printf( "DataInStream: Done...\n" );
  return;
}

/////////////////////////////////////////////////////////////////////////////////////////
//
// Start your implementation by changing this function to implement the game of life
// by farming out parts of the image to worker threads who implement it...
// Currently the function just inverts the image
//
/////////////////////////////////////////////////////////////////////////////////////////
int processCell(int cellAlive, int surroundingLive) {

  if (cellAlive) {
      if (surroundingLive < 2 || surroundingLive > 3) {
          cellAlive = 0;
      }
  } else {
      if (surroundingLive == 3) {
          cellAlive = 1;
      }
  }

  return cellAlive;
}

int findNumCells(int x, int y) {
    int numCells = 8;

    if (x == 0 || x == IMWD) numCells = numCells - 3;
    if (y == 0 || y == IMHT) {
        if (numCells == 8) {
            numCells = numCells - 3;
        } else {
            numCells = numCells - 2;
        }
    }

    return numCells;
}

// Mod that works for values of -b and up
int negSafeMod(int a, int b) {
    return (a + b) % b;
}

//------------------------//
// Bit packing functions  // With help from: http://www.mathcs.emory.edu/~cheung/Courses/255/Syllabus/1-C-intro/bit-array.html
//------------------------//

void setBit(int (&a)[], int x, int y) { //Unsafe
    int i = y /INTSIZE; // quotient of INTSIZE into y = num of ints along from correct x

    unsigned int mask = 1 << (y % INTSIZE); // Remainder = num of bits to shift by inside int

    int index = x*BPHEIGHT + i; // Using x*BPHEIGHT to simulate 2d array

    a[index] |= mask; // Bitwise OR with just a 1 at (x,y) corespondent position, to guarrantee that bit is a 1
}

void clearBit(int (&a)[], int x, int y) { //Technically unsafe
    int i = y / INTSIZE; // quotient of INTSIZE into y = num of ints along from correct x

    // Flip the bits of the mask so there are all 1s except for a 0 at the remainder position
    unsigned int mask = ~(1 << (y % INTSIZE));

    int index = x*BPHEIGHT + i; // Using x*BPHEIGHT to simulate 2d array

    a[index] &= mask; // Bitwise AND with a 0 at (x,y) corespondent position, to guarrantee that bit is a 0
}

int testBit(int (&a)[], int x, int y) {
    int i = y / INTSIZE; // quotient of INTSIZE into y = num of ints along from correct x

    unsigned int flag = 1 << (y % INTSIZE); // Remainder = num of bits to shift by inside int

    int index = x*BPHEIGHT + i; //Using x*BPHEIGHT + i to simulate 2D array

    int result = 0;

    // Evaluate false if there is a 0 in the desired position
    if (a[index] & flag) {
        result = 1;
    }

    return result;
}

uchar bitToUchar(int (&a)[], int x, int y) {
    uchar result = DEAD;

    if (testBit(a, x, y)) {
        result = LIVE;
    }

    return result;
}

int ucharToInt (uchar val) {
    int intval = 1;

    if (val == DEAD) { // If
        intval = 0;
    }

    return intval;
}

void initBitArray(int (&a)[], int size) {
    for( int i = 0; i < size; i++ ) {
        a[i] = 0;
    }
}

void getAdjacentPixels(int workerId, int arrayWidth, int y, int (&bitImg)[], chanend leftWorker, chanend rightWorker) {
    if (workerId % 2){ // Mod 2 So adjacent workers have different and complimentary behaviour
       rightWorker <: bitImg[(arrayWidth - 2)*BPHEIGHT + y];
       rightWorker :> bitImg[(arrayWidth - 1)*BPHEIGHT + y];
       leftWorker <: bitImg[BPHEIGHT + y];
       leftWorker :> bitImg[y];
    } else {
       leftWorker :> bitImg[y];
       leftWorker <: bitImg[BPHEIGHT + y];
       rightWorker :> bitImg[(arrayWidth - 1)*BPHEIGHT + y];
       rightWorker <: bitImg[(arrayWidth - 2)*BPHEIGHT + y];
    }
}

int getLiveSurroundCells(int x, int y, int (&bitImg)[], int arrayWidth) {
    int liveSurroundCells = 0;

    for (int i = -1; i <= 1; i++) {
        for (int j = -1; j <= 1; j++) {
            int newX = negSafeMod(x + i, arrayWidth);
            int newY = negSafeMod(y + j, IMHT);
            int isMidCell = newX == x && newY == y;

            // Don't check cell if it's the middle one
            // or if 4 live surrounding cells have already been found
            // since only knowing whether there are more than 3 live cells is relevant
            int checkCell = !isMidCell && liveSurroundCells < 4;

            // Doesn't waste time on testBit if checkCell fails
            // because of short-circuiting for && operations
            if (checkCell && testBit(bitImg, newX, newY)) {
                liveSurroundCells++;
            }
        }
    }

    return liveSurroundCells;
}

// Processes the cells for a given array and returns the number of live cells
int processCells(int (&bitImg)[], int (&processedImg)[], int arrayWidth) {
    int liveCells = 0;

    for( int y = 0; y < IMHT; y++ ) {
        for( int x = 1; x < arrayWidth - 1; x++ ) {
           int liveSurroundCells = getLiveSurroundCells(x, y, bitImg, arrayWidth);

           //process the cell
           int newCellVal = processCell(testBit(bitImg, x, y), liveSurroundCells);

           //editing bits for the processed image, or else it overlaps with the previous image
           if (newCellVal) {
               liveCells++;
               setBit(processedImg, x, y);
           } else {
               clearBit(processedImg, x, y);
           }
        }
    }

    return liveCells;
}

void copyArray(int (&a)[], int (&b)[], int width, int height) {
    for (int y = 0; y < height; y++ ) {
       for (int x = 0; x < width; x++) {
           a[x*height + y] = b[x*height + y];
       }
    }
}

void worker(chanend farmer, chanend leftWorker, chanend rightWorker, int workerId) {
    //Initialise worker

    int arrayWidth = IMWD/NUMWORKERS + 2; // '+ 2' for extra strip of pixels

    int bitImg[(IMWD/NUMWORKERS + 2)*BPHEIGHT]; // Can't use variables to declare array size :( Maybe use int* + malloc (risky)
    int processedImg[(IMWD/NUMWORKERS + 2)*BPHEIGHT];

    initBitArray(bitImg, (IMWD/NUMWORKERS + 2)*BPHEIGHT);
    initBitArray(processedImg, (IMWD/NUMWORKERS + 2)*BPHEIGHT);

    // Recieve pixel data from farmer
    for( int y = 0; y < IMHT; y++ ) {
        for( int x = 1; x < arrayWidth - 1; x++ ) {
            int val = 0;
            farmer :> val;
            if (val) {
                setBit(bitImg, x, y);  //Making bitImg
            }
        }
    }

    // Processing loop continues indefinitely
    while(1) {

        // Get pixel strips from neighbouring workers
        for (int y = 0; y < BPHEIGHT; y++){
            getAdjacentPixels(workerId, arrayWidth, y, bitImg, leftWorker, rightWorker);
        }

        // Let the farmer know the worker has finished the round
        // Send the farmer the number of live cells at the same time with the return value of processCells
        farmer <: processCells(bitImg, processedImg, arrayWidth);

        // Store processedImg to bitImg
        copyArray(bitImg, processedImg, arrayWidth, BPHEIGHT);

        int instruction;
        farmer :> instruction; // Wait for instruction from farmer

        // If farmer wants to recieve processed data, send it
        if (instruction == 13) {
            for (int y = 0; y < BPHEIGHT; y++ ) {
                for (int x = 1; x < arrayWidth - 1; x++) {
                    farmer <: processedImg[x*BPHEIGHT + y];
                }
            }
        }
    }
}

uint32_t toMilliseconds (uint32_t time) {
    return time/100000;
}

double calcElapsedTime(uint32_t startTime, uint32_t endTime) {
    uint32_t time = toMilliseconds(endTime - startTime);

    if (endTime < startTime) {
        time += toMilliseconds(OVERFLOWTIME);
    }

    return time;
}

void sendDataToOutStream(chanend worker[NUMWORKERS], chanend c_out, int (&bitImg)[], int round, chanend toLEDS) {

    //exporting images
    toLEDS <: BLUE;

    // Get data from workers
    for (int i = 0; i < NUMWORKERS; i++) {
        worker[i] <: (13);
        for (int y = 0; y < BPHEIGHT; y++ ) {
            for (int x = i * (IMWD/NUMWORKERS); x < (i+1) * (IMWD/NUMWORKERS); x++) { // Shift index so the farmed data is stored correctly
                worker[i] :> bitImg[x*BPHEIGHT + y];
            }
        }
    }

    // Send round number to c_out to be used for the file name
    c_out <: round;

    for( int y = 0; y < IMHT; y++ ) {   //go through all lines
        for( int x = 0; x < IMWD; x++ ) { //go through each pixel per line
            c_out <: bitToUchar(bitImg, x, y);
        }
    }
}

void nextRound(int &liveCells, int &newLiveCells, int &workersDone, chanend toLEDS, int &round, uint32_t &startTime, timer t, uint32_t &totalTime) {
    liveCells = newLiveCells;
    newLiveCells = 0;
    workersDone = 0;

    uint32_t endTime;
    t :> endTime;
    totalTime += calcElapsedTime(startTime, endTime);
    t :> startTime;

    int pattern = 0;
    if (round%2) pattern = SGREEN;

    toLEDS <: pattern;

    //For benchmarking purposes
    if ((round == 2 || round == 1002) && TEST){
        printf("Total time for %d rounds with %d workers: %u\n", round, NUMWORKERS, totalTime);
    }

    round++;
}

void distributor(chanend c_in, chanend c_out, chanend fromAcc, chanend worker[NUMWORKERS], chanend fromButtons, chanend toLEDS){
  uchar val;
  timer t;
  uint32_t startTime, endTime, totalTime = 0;

  int liveCells = 0;

  // Create and initialise bit array so every element is 0
  int bitImg[IMWD * BPHEIGHT];
  initBitArray(bitImg, IMWD * BPHEIGHT);

  //Starting up and wait for button press of SW1
  int buttonInput = 0;
  while (buttonInput != 14) fromButtons :> buttonInput;


  toLEDS <: GREEN;
  printf( "Reading and processing start: size = %dx%d\n", IMHT, IMWD );

  // Recieve initial image
  for( int y = 0; y < IMHT; y++ ) {   //go through all lines
      for( int x = 0; x < IMWD; x++ ) { //go through each pixel per line
        c_in :> val;                    //read the pixel value

        if (val == LIVE) liveCells++; // Count live cells in initial image

        int workerIndex = x/(IMWD/NUMWORKERS); // 0 <= x < NUMWORKERS goes to worker[0], NUMWORKERS <= x < 2*NUMWORKERS goes to worker[1] etc.
        worker[workerIndex] <: ucharToInt(val);
      }
  }

  if(!TEST) printf("Number of liveCells: %d\n", liveCells);
  t :> startTime;

  // newLiveCells so that liveCells data not lost when building up data for next round
  int round = 1, workersDone = 0, newLiveCells = 0;

  while (1){

      select {
          case fromAcc :> int val:
              toLEDS <: RED;
              t :> endTime;
              totalTime += calcElapsedTime(startTime, endTime);
              printf("Round: %d, Current number of live cells: %d, Time: %u\n", round, liveCells, totalTime);

              fromAcc :> val;
              t :> startTime;

              break;

          case fromButtons :> buttonInput:
                if (buttonInput == 13){

                    while (workersDone < NUMWORKERS) {
                        select {
                            case worker[int i] :> int workerLiveCells:
                                workersDone++;
                                newLiveCells += workerLiveCells;

                                break;
                        }
                    }

                    printf("Round %d processed\n", round);

                    sendDataToOutStream(worker, c_out, bitImg, round, toLEDS);

                    nextRound(liveCells, newLiveCells, workersDone, toLEDS, round, startTime, t, totalTime);
                }

                break;

          case worker[int i] :> int workerLiveCells:
              newLiveCells += workerLiveCells;

              workersDone++;
              if (workersDone == NUMWORKERS) {
                  if (!TEST) printf("Round %d processed\n", round);

                  if (((round % 100 == 0 || round == 2) && !TEST) || ((TEST == 2) && (round % 1000 == 0 || round == 2))) {
                      sendDataToOutStream(worker, c_out, bitImg, round, toLEDS);
                  } else {
                      for (int j = 0; j < NUMWORKERS; j++) worker[j] <: 0; // Tell workers to do next round
                  }

                  nextRound(liveCells, newLiveCells, workersDone, toLEDS, round, startTime, t, totalTime);
              }
              break;
      }
  }
}
/////////////////////////////////////////////////////////////////////////////////////////
//
// Write pixel stream from channel c_in to PGM image file
//
/////////////////////////////////////////////////////////////////////////////////////////

void getFileName(char *fileName, int round) {
    snprintf(fileName, 32, "round%d.pgm", round);
}

void DataOutStream(chanend c_in)
{
  int res;
  uchar line[ IMWD ];

  char *fileName = malloc(10 * sizeof(char)); // don't need to free since fileName will always be in use

  printf( "DataOutStream: Start...\n" );

  while (1) {

    int round = 0;
    c_in :> round;

    getFileName(fileName, round);

    //Open PGM file
    printf("Opening file: %s\n", fileName);
    res = _openoutpgm( fileName, IMWD, IMHT );

    if( res ) {
      printf( "DataOutStream: Error opening %s\n.", fileName );
      return;
    }

    //Compile each line of the image and write the image line-by-line
    for( int y = 0; y < IMHT; y++ ) {
      for( int x = 0; x < IMWD; x++ ) {
        c_in :> line[ x ];
      }
      _writeoutline( line, IMWD );
    }

    //Close the PGM image
    _closeoutpgm();
    printf( "DataOutStream: Done...\n" );

  }
    //return;
}


/////////////////////////////////////////////////////////////////////////////////////////
//
// Initialise and  read orientation, send first tilt event to channel
//
/////////////////////////////////////////////////////////////////////////////////////////
void orientation( client interface i2c_master_if i2c, chanend toDist) {
  i2c_regop_res_t result;
  char status_data = 0;
  int tilted = 0;

  // Configure FXOS8700EQ
  result = i2c.write_reg(FXOS8700EQ_I2C_ADDR, FXOS8700EQ_XYZ_DATA_CFG_REG, 0x01);
  if (result != I2C_REGOP_SUCCESS) {
    printf("I2C write reg failed\n");
  }
  
  // Enable FXOS8700EQ
  result = i2c.write_reg(FXOS8700EQ_I2C_ADDR, FXOS8700EQ_CTRL_REG_1, 0x01);
  if (result != I2C_REGOP_SUCCESS) {
    printf("I2C write reg failed\n");
  }

  //Probe the orientation x-axis forever
  while (1) {

    //check until new orientation data is available
    do {
      status_data = i2c.read_reg(FXOS8700EQ_I2C_ADDR, FXOS8700EQ_DR_STATUS, result);
    } while (!status_data & 0x08);

    //get new x-axis tilt value
    int x = read_acceleration(i2c, FXOS8700EQ_OUT_X_MSB);

    int tippingPoint = 40;

    //send signal to distributor after first tilt

    if (tilted) {
        // (-10) to give a bit of leeway for pausing/unpausing
        // Otherwise x == tippingPoint will pause,
        // then dropping very slightly to x == tippingPoint - 1 will unpause etc
        if (x <= (tippingPoint - 10)) {
            toDist <: 0;
            tilted = 0;
        }
    } else {
        if (x > tippingPoint) {
            toDist <: 1;
            tilted = 1;
        }
    }

  }
}

/////////////////////////////////////////////////////////////////////////////////////////
//
// Orchestrate concurrent system and start up all threads
//
/////////////////////////////////////////////////////////////////////////////////////////
int main(void) {

i2c_master_if i2c[1];               //interface to orientation


chan c_inIO, c_outIO, c_control;    //extend your channel definitions here
chan f[NUMWORKERS];
chan c[NUMWORKERS][NUMWORKERS];
chan buttonsToDistributor, distributorToLEDs;

par {
    on tile [0]: i2c_master(i2c, 1, p_scl, p_sda, 10);   //server thread providing orientation data
    on tile [0]: orientation(i2c[0],c_control);        //client thread reading orientation data
    on tile [0]: DataInStream("test_16.pgm", c_inIO);          //thread to read in a PGM image
    on tile [0]: DataOutStream(c_outIO);       //thread to write out a PGM image
    on tile [0]: distributor(c_inIO, c_outIO, c_control, f, buttonsToDistributor, distributorToLEDs);//thread to coordinate work on image
    on tile [0]: worker (f[0], c[NUMWORKERS - 1][0], c[0][1], 0);

    on tile [1]: worker (f[1], c[0][1], c[1][0], 1);
    on tile [1]: worker (f[2], c[1][2], c[2][0], 2);
    on tile [1]: worker (f[3], c[2][3], c[3][0], 3);
//    on tile [1]: worker (f[4], c[3][4], c[4][5], 4);
//    on tile [1]: worker (f[5], c[4][5], c[5][6], 5);
//    on tile [1]: worker (f[6], c[5][6], c[6][7], 6);
//    on tile [1]: worker (f[7], c[6][7], c[7][0], 7);

    on tile [0]: buttonListener(buttons, buttonsToDistributor);
    on tile [0]: showLEDs(leds, distributorToLEDs);
  }

  return 0;
}

